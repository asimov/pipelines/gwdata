Accessing gravitational wave data with asimov
=============================================

asimov-gwdata is a pipeline designed to work with the asimov package in order to collect various bits of gravitational wave data which might be used in subsequent analyses.

It's designed to work as part of a larger workflow which conducts analyses on the data, while simplifying the process of finding and collecting various data products which might be used across a variety of analysis tasks.
It's not really intended as a stand-alone pipeline, but you will see it being used in, for example, tutorials on analysing gravitational wave events in open data.

.. toctree::
   :maxdepth: 2
   :caption: User Guide

   installation
   frames
   calibration
   pesummary


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
